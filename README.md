# coinbase-bot

## References

- https://www.npmjs.com/package/ccxws
- https://www.npmjs.com/package/ccxt
- https://github.com/ccxt/ccxt/wiki/Manual

## Results from API

- Create order

```js
{ id: 'c28f0326-9e3d-4189-8c53-897f72ee7cf7',
  info:
   { id: 'c28f0326-9e3d-4189-8c53-897f72ee7cf7',
     price: '500',
     size: '0.01',
     product_id: 'ETH-EUR',
     side: 'sell',
     stp: 'dc',
     type: 'limit',
     time_in_force: 'GTC',
     post_only: false,
     created_at: '2020-02-21T08:49:25.343699Z',
     fill_fees: '0',
     filled_size: '0',
     executed_value: '0',
     status: 'pending',
     settled: false },
  timestamp: 1582274965343,
  datetime: '2020-02-21T08:49:25.343Z',
  lastTradeTimestamp: undefined,
  status: 'open',
  symbol: 'ETH/EUR',
  type: 'limit',
  side: 'sell',
  price: 500,
  cost: 0,
  amount: 0.01,
  filled: 0,
  remaining: 0.01,
  fee: { cost: 0, currency: 'EUR', rate: undefined } }
  ```

  - Cancel Order

  ```
  "c28f0326-9e3d-4189-8c53-897f72ee7cf7"
  ```

  ## Results from Websocket

  - Ticker

  ```
  Ticker {
  exchange: 'CoinbasePro',
  base: 'ETH',
  quote: 'EUR',
  timestamp: 1582258806225,
  last: '243.37',
  open: '241.53000000',
  high: '244.90000000',
  low: '228.43000000',
  volume: '36520.62381410',
  quoteVolume: undefined,
  change: '1.84000000',
  changePercent: '0.76181013',
  bid: '242.94',
  bidVolume: undefined,
  ask: '243.37',
  askVolume: undefined }
  ```

  - Trade

  ```
  Trade {
  exchange: 'CoinbasePro',
  quote: 'EUR',
  base: 'ETH',
  tradeId: '8116860',
  unix: 1582258885337,
  side: 'buy',
  price: '242.7',
  amount: '0.4658916',
  buyOrderId: 'ea7bad62cede4165b2f9c0c6605874ff',
  sellOrderId: '39eb523396f8407d8667fd141cd9990f' }
  ```