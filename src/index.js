require("dotenv").config();

const {
  init: initExchange,
  getOpenOrders,
  getBalance,
  cancelOrder
} = require("./exchange");
const { init: initLive } = require("./live");
const { placeSellOrder } = require("./trader");

const MARKET = "ETH/EUR";
const MARKET_DATA = {
  id: "ETH-EUR", // remote_id used by the exchange
  base: "ETH", // standardized base symbol for Bitcoin
  quote: "EUR" // standardized quote symbol for Tether
};

const start = async function() {
  await Promise.all([initExchange() /*initLive(MARKET_DATA)*/]);
  console.debug("INITIALIZED");
  let openOrders = await getOpenOrders(MARKET);
  console.log(openOrders);
  const balance = await getBalance();
  console.log(balance);
  // const result = await placeSellOrder(MARKET, 0.01, 500);
  // console.log(result);
  // openOrders = await getOpenOrders(MARKET);
  // console.log(openOrders);

  // if (openOrders[1].id === "xxx") {
  //   const result = await cancelOrder(openOrders[1].id, openOrders[1].symbol);
  //   console.log(result);
  // }
};

start();
