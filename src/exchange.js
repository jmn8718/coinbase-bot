const ccxt = require("ccxt");
const pick = require("lodash.pick");
const { ORDER_TYPES } = require("./constants");

const {
  COINBASE_API_KEY,
  COINBASE_PASSWORD,
  COINBASE_SECRET
} = require("./config");

let coinbasepro;
const init = async function() {
  coinbasepro = new ccxt.coinbasepro({
    apiKey: COINBASE_API_KEY,
    secret: COINBASE_SECRET,
    password: COINBASE_PASSWORD,
    enableRateLimit: true
  });
  console.debug("CONNECTED TO COINBASE");
};

const getOpenOrders = function(market) {
  return coinbasepro.fetchOpenOrders(market);
};

const generateOrderData = function(
  amount,
  price,
  side,
  type = ORDER_TYPES.LIMIT
) {
  return {
    type,
    side,
    amount,
    price,
    postOnly: true
  };
};

const createOrder = function(market, order) {
  return coinbasepro.createOrder(
    market,
    order.type,
    order.side,
    order.amount,
    order.price,
    order.params
  );
};

const cancelOrder = async function(orderId, market) {
  return coinbasepro.cancelOrder(orderId, market);
};

const getBalance = async function() {
  const balances = await coinbasepro.fetchBalance();
  return pick(balances, ["EUR", "ETH", "BTC"]);
};

module.exports = {
  init,
  getBalance,
  getOpenOrders,
  createOrder,
  cancelOrder,
  generateOrderData
};
