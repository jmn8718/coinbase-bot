const dotenv = require("dotenv");

const result = dotenv.config();

if (result.error) {
  throw result.error;
}

const CONFIG = {
  COINBASE_API_KEY: process.env.COINBASE_API_KEY,
  COINBASE_SECRET: process.env.COINBASE_SECRET,
  COINBASE_PASSWORD: process.env.COINBASE_PASSWORD
};

module.exports = CONFIG;
