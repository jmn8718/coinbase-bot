const ccxws = require("ccxws");
const get = require("lodash.get");
const set = require("lodash.set");

const store = {};

const TYPES = {
  TICKER: "ticker",
  TRADE: "trade"
};

const log = data => console.log(JSON.stringify(data));
const logUpdate = function(type, data) {
  if (type === TYPES.TICKER) {
    const bid = get(data, "bid");
    const ask = get(data, "ask");
    const timestamp = get(data, "timestamp");
    log({ type, timestamp, bid, ask });
  } else if (type === TYPES.TRADE) {
    const side = get(data, "side");
    const price = get(data, "price");
    const amount = get(data, "amount");
    const timestamp = get(data, "unix");
    log({ type, timestamp, side, price, amount });
  }
};

const updateStore = type => data => {
  set(store, type, data);

  logUpdate(type, data);
};

const init = async function(market) {
  const coinbasepro = new ccxws.coinbasepro();

  coinbasepro.on("connecting", () => console.debug("LIVE CONNECTING..."));
  coinbasepro.on("connected", () => console.debug("LIVE CONNECTED"));
  coinbasepro.on("disconnected", () => console.debug("LIVE DISCONNECTED"));
  coinbasepro.on("reconnecting", () => console.debug("LIVE RECONNECTING"));
  coinbasepro.on("error", err => console.error("LIVE ", err));

  coinbasepro.on("ticker", updateStore(TYPES.TICKER));
  coinbasepro.on("trade", updateStore(TYPES.TRADE));

  // subscribe to trades
  coinbasepro.subscribeTrades(market);
  coinbasepro.subscribeTicker(market);
};

module.exports = {
  init
};
