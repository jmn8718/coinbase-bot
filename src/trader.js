const { ORDER_SIDES, ORDER_TYPES } = require("./constants");
const { generateOrderData, createOrder } = require("./exchange");

const placeSellOrder = async function(market, amount, price) {
  const order = generateOrderData(
    amount,
    price,
    ORDER_SIDES.SELL,
    ORDER_TYPES.LIMIT
  );
  const status = await createOrder(market, order);
  return status;
};

module.exports = {
  placeSellOrder
};
