module.exports.ORDER_TYPES = {
  LIMIT: "limit",
  MARKET: "market"
};

module.exports.ORDER_SIDES = {
  BUY: "buy",
  SELL: "sell"
};
